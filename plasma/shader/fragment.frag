#version 430 core
in vec2 uv;

out vec4 fragColor;

uniform float uAspectRatio;
uniform uint uNx;
uniform uint uNy;
uniform float uNorm;

layout (std430, binding = 4) restrict readonly buffer Rho { float data[]; } rho;
layout (std430, binding = 5) restrict readonly buffer Phi { float data[]; } phi;

//const vec3 color[6] = vec3[6](
//    vec3(0.f, 0.f, 1.f),
//    vec3(0.f, 1.f, 1.f),
//    vec3(0.f, 1.f, 0.f),
//    vec3(1.f, 1.f, 0.f),
//    vec3(1.f, 0.f, 0.f),
//    vec3(1.f, 0.f, 1.f));
//
//vec3 colormap(float t) {
//    float f = t * 6.f;
//    int I0 = int(f);
//    int I1 = min(5, I0 + 1);
//    return mix(color[I0], color[I1], f - floor(f));
//}

void main() {
    uint i = uint(0.5f * (1.f + uv.x) * float(uNx));
    uint j = uint(0.5f * (1.f + uv.y) * float(uNy));
    float x = phi.data[i + j * uNx] / uNorm;
    float r = x > 0.f ? x : 0.f;
    float b = x < 0.f ? -x : 0.f;

    fragColor = vec4(r, 0.f, b, 1.f);
}
