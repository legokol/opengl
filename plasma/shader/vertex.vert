#version 430 core
out vec2 uv;

void main() {
    uv = vec2[](
        vec2(-1.f, -1.f),
        vec2(1.f, -1.f),
        vec2(-1.f, 1.f),
        vec2(1.f, -1.f),
        vec2(-1.f, 1.f),
        vec2(1.f, 1.f)
    )[gl_VertexID];
    gl_Position = vec4(uv, 0.f, 1.f);
}
