#include <algorithm>
#include <array>
#include <cmath>
#include <cstring>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <numbers>
#include <source_location>
#include <vector>

#include "lgklGL/Shader.hpp"
#include "lgklGL/Window.hpp"

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

int main() try {
    const std::size_t nx = 256;
    const std::size_t ny = 200;

    const float step = 3e-5;

    const float Lx = nx * step;
    const float Ly = (ny - 1) * step;

    const std::size_t numberOfParticles = 5'000'000;

    std::vector<float> x(2 * numberOfParticles);
    std::vector<float> y(2 * numberOfParticles);
    std::vector<float> vx(2 * numberOfParticles);
    std::vector<float> vy(2 * numberOfParticles);
    std::vector<float> charge(2 * numberOfParticles);

    const std::size_t mode = 2;
    const float lambda     = Lx / static_cast<float>(mode);
    const float k          = 2.f * std::numbers::pi_v<float> / lambda;

    const std::size_t yParticlesNumber = 400;
    const std::size_t xParticlesNumber = numberOfParticles / yParticlesNumber;
    const float xParticleStep          = Lx / static_cast<float>(xParticlesNumber);
    const float yParticleStep          = (Ly - step) / static_cast<float>(yParticlesNumber - 1);
    const float dx                     = 0.1f * step;

    for (std::size_t j = 0; j < yParticlesNumber; ++j) {
        const float y0 = 0.5f * step + static_cast<float>(j) * yParticleStep;
        for (std::size_t i = 0; i < xParticlesNumber; ++i) {
            const std::size_t idx = i + j * xParticlesNumber;
            const float x0        = (static_cast<float>(i) + 0.5f) * xParticleStep;
            x[idx]                = (x0 + dx * std::sin(k * x0)) / step;
            if (x[idx] < 0) {
                x[idx] += static_cast<float>(nx);
            }
            if (x[idx] >= static_cast<float>(nx)) {
                x[idx] -= static_cast<float>(nx);
            }
            y[idx] = y0 / step;

            x[numberOfParticles + idx] = x0 / step;
            y[numberOfParticles + idx] = y0 / step;

            vx[idx] = 0.f;
            vy[idx] = 0.f;

            charge[idx]                     = -1.f;
            charge[numberOfParticles + idx] = 1.f;
        }
    }

    const float m_e      = 1.f;
    const float e        = 1.f;
    const float eps0     = 1.f;
    const float n0       = 1e17;
    const float w        = n0 * Lx * Ly / static_cast<float>(numberOfParticles);
    const float timeStep = 1e-4f / std::sqrt(m_e * e * e / eps0);

    const float m       = w * m_e;
    const float m0      = m;
    const float q0      = e;
    const float rho0    = m0 / q0 * (eps0 / timeStep / timeStep);
    const float mRho    = q0 / (rho0 * step * step);
    const float phiNorm = 2e2f * mRho;

    const Window window(800, 600, "Plasma");

    // paths to shaders
    const std::string filePath = std::source_location::current().file_name();
    const auto pos             = filePath.find("plasma.cpp");
    const auto path            = std::filesystem::path(filePath.substr(0, pos)).make_preferred();
    const auto shaderPath      = path / "shader";

    // shaders
    const Shader move{shaderPath / "move.comp"};
    const Shader clear{shaderPath / "clear.comp"};
    const Shader weight{shaderPath / "weight.comp"};
    const Shader fft{shaderPath / "fft.comp"};
    const Shader ifft{shaderPath / "ifft.comp"};
    const Shader thomas{shaderPath / "thomas.comp"};
    const Shader Ex{shaderPath / "Ex.comp"};
    const Shader Ey{shaderPath / "Ey.comp"};
    const Shader graphics{shaderPath / "vertex.vert", shaderPath / "fragment.frag"};

    // vertex array
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    GLuint buffer[12];
    glGenBuffers(12, buffer);

    {
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer[0]);
        glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(float) * 2 * numberOfParticles, x.data(), GL_DYNAMIC_DRAW);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, buffer[0]);

        glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer[1]);
        glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(float) * 2 * numberOfParticles, y.data(), GL_DYNAMIC_DRAW);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, buffer[1]);

        glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer[2]);
        glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(float) * 2 * numberOfParticles, vx.data(), GL_DYNAMIC_DRAW);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, buffer[2]);

        glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer[3]);
        glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(float) * 2 * numberOfParticles, vy.data(), GL_DYNAMIC_DRAW);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, buffer[3]);

        glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer[11]);
        glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(float) * 2 * numberOfParticles, charge.data(), GL_DYNAMIC_DRAW);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 11, buffer[11]);
    }

    for (std::size_t i = 4; i < 8; ++i) {
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer[i]);
        glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(float) * nx * ny, nullptr, GL_DYNAMIC_DRAW);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, i, buffer[i]);
    }
    {
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer[8]);
        glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(float) * 2 * nx * ny, nullptr, GL_DYNAMIC_DRAW);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 8, buffer[8]);

        glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer[9]);
        glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(float) * nx * (ny - 3), nullptr, GL_DYNAMIC_DRAW);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 9, buffer[9]);

        glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer[10]);
        glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(float) * 2 * nx * (ny - 3), nullptr, GL_DYNAMIC_DRAW);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 10, buffer[10]);
    }

    const auto solve = [&] {
        // weight particles
        {
            glUseProgram(clear.program);
            glUniform1ui(glGetUniformLocation(clear.program, "uN"), nx * ny);
            glDispatchCompute((nx * ny + 31) / 32, 1, 1);
            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

            glUseProgram(weight.program);
            glUniform1ui(glGetUniformLocation(weight.program, "uParticleCount"), 2 * numberOfParticles);
            glUniform1ui(glGetUniformLocation(weight.program, "uNx"), nx);

            glDispatchCompute((2 * numberOfParticles + 31) / 32, 1, 1);
            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        }

        // fft
        {
            glUseProgram(fft.program);
            glUniform1ui(glGetUniformLocation(fft.program, "N"), nx);
            glUniform1ui(glGetUniformLocation(fft.program, "log2N"), 8);  // очень плохо
            glUniform1f(glGetUniformLocation(fft.program, "m"), mRho);

            glDispatchCompute(ny - 2, 1, 1);
            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        }
        // solve tridiagonal
        {
            glUseProgram(thomas.program);
            glUniform1ui(glGetUniformLocation(thomas.program, "uNx"), nx);
            glUniform1ui(glGetUniformLocation(thomas.program, "uNy"), ny);

            glDispatchCompute((nx + 31) / 32, 1, 1);
            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        }
        // ifft
        {
            glUseProgram(ifft.program);
            glUniform1ui(glGetUniformLocation(ifft.program, "N"), nx);
            glUniform1ui(glGetUniformLocation(ifft.program, "log2N"), 8);  // очень плохо

            glDispatchCompute(ny - 2, 1, 1);
            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        }
        // Ex
        {
            glUseProgram(Ex.program);
            glUniform1ui(glGetUniformLocation(Ex.program, "uNx"), nx);

            glDispatchCompute(ny - 2, 1, 1);
            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        }
        // Ey
        {
            glUseProgram(Ey.program);
            glUniform1ui(glGetUniformLocation(Ey.program, "uNx"), nx);
            glUniform1ui(glGetUniformLocation(Ey.program, "uNy"), ny);

            glDispatchCompute(ny, 1, 1);
            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        }
    };
    const auto write = [&](std::size_t idx, const std::string &name) {
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer[idx]);
        void *prho = glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);
        std::vector<float> data(nx * ny);
        std::memcpy(data.data(), prho, sizeof(float) * nx * ny);
        glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

        std::ofstream writer(name, std::ios::binary);
        writer.write(reinterpret_cast<const char *>(data.data()), data.size() * sizeof(float));
    };

    const auto moveP = [&] {
        // move particles
        {
            glUseProgram(move.program);
            glUniform1ui(glGetUniformLocation(move.program, "uParticleCount"), numberOfParticles);
            glUniform1ui(glGetUniformLocation(move.program, "uNx"), nx);
            glUniform1ui(glGetUniformLocation(move.program, "uNy"), ny);

            glDispatchCompute((numberOfParticles + 31) / 32, 1, 1);
            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        }
    };

    solve();

    // write(4, "rho0.raw");
    // write(5, "phi0.raw");
    // write(6, "Ex0.raw");
    // write(7, "Ey0.raw");

    // render loop
    while (!window.shoudClose()) {
        // key input
        processInput(window.pointer);

        glBindVertexArray(VAO);

        moveP();
        solve();

        // fill background
        glClearColor(0.f, 0.f, 0.f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // draw
        glUseProgram(graphics.program);

        int width, height;
        glfwGetFramebufferSize(window.pointer, &width, &height);
        glUniform1f(glGetUniformLocation(graphics.program, "uAspectRatio"),
                    static_cast<float>(width) / static_cast<float>(height));
        glUniform1ui(glGetUniformLocation(graphics.program, "uNx"), nx);
        glUniform1ui(glGetUniformLocation(graphics.program, "uNy"), ny);
        glUniform1f(glGetUniformLocation(graphics.program, "uNorm"), phiNorm);
        glDrawArrays(GL_TRIANGLES, 0, 6);
    }

    glDeleteVertexArrays(1, &VAO);

    graphics.terminate();
    window.terminate();

    return 0;
} catch (std::exception &e) {
    std::cout << e.what() << std::endl;
}
