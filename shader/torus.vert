#version 330 core
layout (location = 0) in vec3 uPos;
layout (location = 1) in vec2 uTexCoord;

out vec2 texCoord;

uniform float t;
uniform float uAspectRatio;

void main() {
    // projection parameters:
    float ctgFOV = 1.5f;
    float f = 100.f;
    float n = 0.5f;

    float C1 = (f + n) / (f - n);
    float C2 = 2.f * f * n / (f - n);

    float a = -t;
    mat4 rotation = mat4(
        1.f, 0.f, 0.f, 0.f,
        0.f, cos(a), sin(a), 0.f,
        0.f, -sin(a), cos(a), 0.f,
        0.f, 0.f, 0.f, 1.f
    );
    mat4 view = mat4(
        1.f, 0.f, 0.f, 0.f,
        0.f, 1.f, 0.f, 0.f,
        0.f, 0.f, 1.f, 0.f,
        0.f, 0.f, -2.3f, 1.f
    );
    mat4 perspective = mat4(
        ctgFOV / uAspectRatio, 0.f, 0.f, 0.f,
        0.f, ctgFOV, 0.f, 0.f,
        0.f, 0.f, -C1, -1.f,
        0.f, 0.f, -C2, 0.f
    );
    gl_Position = perspective * view * rotation * vec4(uPos, 1.f);
    texCoord = uTexCoord;
}
