#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;

out vec2 texCoord;

uniform float t;

void main() {
    mat4 rotation = mat4(
        cos(t), sin(t), 0, 0,
        -sin(t), cos(t), 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    );
    gl_Position = rotation * vec4(aPos, 1.0);
    texCoord = aTexCoord;
}
