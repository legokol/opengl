#pragma once

#include <string_view>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

struct Window {
    GLFWwindow* pointer;

    Window(int width, int height, std::string_view title);

    [[nodiscard]] bool shoudClose() const {
        glfwPollEvents();
        glfwSwapBuffers(pointer);
        return glfwWindowShouldClose(pointer);
    }

    void terminate() const {
        glfwDestroyWindow(pointer);
        glfwTerminate();
    }
};
inline Window::Window(int width, int height, std::string_view title) {
    glfwInit();

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // GLFW window creation
    pointer = glfwCreateWindow(width, height, title.data(), nullptr, nullptr);
    if (!pointer) {
        throw std::runtime_error("Failed to create GLFW window");
    }
    glfwMakeContextCurrent(pointer);

    // glad initialization
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        throw std::runtime_error("Failed to initialize GLAD");
    }

    glfwSetFramebufferSizeCallback(
        pointer, +[](GLFWwindow*, int width, int height) { glViewport(0, 0, width, height); });
}
