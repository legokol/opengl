#pragma once

#include <filesystem>
#include <fstream>

#include <glad/glad.h>

class Shader {
public:
    unsigned int program;

    explicit Shader(const char* compute);
    explicit Shader(const std::filesystem::path& computePath);

    Shader(const char* vertex, const char* fragment);
    Shader(const std::filesystem::path& vertexPath, const std::filesystem::path& fragmentPath);

    void terminate() const { glDeleteProgram(program); }

private:
    [[nodiscard]] inline std::string readShader(const std::filesystem::path& path) {
        std::ifstream in(path);
        char buffer[16384] = {'\0'};
        in.read(buffer, sizeof(buffer));
        return std::string{buffer};
    }

    [[nodiscard]] GLuint compileShader(GLenum const kind, const char* src) {
        GLuint shader = glCreateShader(kind);
        glShaderSource(shader, 1, &src, nullptr);
        glCompileShader(shader);

        GLint isCreated;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &isCreated);
        if (isCreated != GL_TRUE) {
            GLint size = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &size);
            std::string err(size, '\0');
            glGetShaderInfoLog(shader, size, nullptr, err.data());
            throw std::runtime_error("Failed to compile shader\n" + err);
        }
        return shader;
    }

    void checkShaderLinkage(GLuint program) {
        GLint linked;
        glGetProgramiv(program, GL_LINK_STATUS, &linked);
        if (linked != GL_TRUE) {
            GLint size = 0;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &size);
            std::string err(std::size_t(size), '\0');
            glGetProgramInfoLog(program, size, nullptr, err.data());
            throw std::runtime_error("Failed to link shader: " + err);
        }
    }
};

inline Shader::Shader(const char* compute) : program{glCreateProgram()} {
    const GLuint computeShader = compileShader(GL_COMPUTE_SHADER, compute);
    glAttachShader(program, computeShader);
    glLinkProgram(program);
    glDeleteShader(computeShader);
    checkShaderLinkage(program);
}
inline Shader::Shader(const std::filesystem::path& computePath) : program{glCreateProgram()} {
    const std::string computeShaderStr = readShader(computePath);
    const char* compute                = computeShaderStr.c_str();

    const GLuint computeShader = compileShader(GL_COMPUTE_SHADER, compute);
    glAttachShader(program, computeShader);
    glLinkProgram(program);
    glDeleteShader(computeShader);
    checkShaderLinkage(program);
}

inline Shader::Shader(const char* vertex, const char* fragment) : program{glCreateProgram()} {
    const GLuint vertexShader   = compileShader(GL_VERTEX_SHADER, vertex);
    const GLuint fragmentShader = compileShader(GL_FRAGMENT_SHADER, fragment);

    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);

    glLinkProgram(program);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    checkShaderLinkage(program);
}

inline Shader::Shader(const std::filesystem::path& vertexPath, const std::filesystem::path& fragmentPath)
    : program{glCreateProgram()} {
    const std::string vertexShaderStr   = readShader(vertexPath);
    const char* vertex                  = vertexShaderStr.c_str();
    const std::string fragmentShaderStr = readShader(fragmentPath);
    const char* fragment                = fragmentShaderStr.c_str();

    const GLuint vertexShader   = compileShader(GL_VERTEX_SHADER, vertex);
    const GLuint fragmentShader = compileShader(GL_FRAGMENT_SHADER, fragment);

    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);

    glLinkProgram(program);
    checkShaderLinkage(program);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}
