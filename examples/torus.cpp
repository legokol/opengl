#include <chrono>
#include <cmath>
#include <filesystem>
#include <iostream>
#include <numbers>
#include <source_location>
#include <vector>

#include "lgklGL/stb_image.h"

#include "lgklGL/Shader.hpp"
#include "lgklGL/Window.hpp"

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

int main() try {
    const Window window(800, 600, "Triangle");

    // triangle vertices with texture coords
    const float R          = 0.8;
    const float r          = 0.4;
    const std::size_t Nphi = 100;
    const std::size_t Npsi = 100;
    std::vector<float> vertices(5 * Nphi * Npsi);  // vertices and texture coords
    for (std::size_t i = 0; i < Nphi; ++i) {
        const float xt   = static_cast<float>(i) / static_cast<float>(Nphi);
        const float phi  = 2 * std::numbers::pi_v<float> * (xt + 0.25);
        const float sPhi = std::sin(phi);
        const float cPhi = std::cos(phi);
        for (std::size_t j = 0; j < Npsi; ++j) {
            const float yt  = static_cast<float>(j) / static_cast<float>(Npsi);
            const float psi = 2 * std::numbers::pi_v<float> * (yt - 0.25);

            // vertex
            vertices[5 * (i * Npsi + j)]     = (R + r * std::cos(psi)) * cPhi;
            vertices[5 * (i * Npsi + j) + 1] = (R + r * std::cos(psi)) * sPhi;
            vertices[5 * (i * Npsi + j) + 2] = r * std::sin(psi);

            // texture
            vertices[5 * (i * Npsi + j) + 3] = xt;
            vertices[5 * (i * Npsi + j) + 4] = yt;
        }
    }
    // indices for triangles
    std::vector<unsigned> indices(6 * Nphi * Npsi);
    for (std::size_t i = 0; i < Nphi; ++i) {
        for (std::size_t j = 0; j < Npsi; ++j) {
            indices[6 * (i * Nphi + j)]     = i * Nphi + j;
            indices[6 * (i * Nphi + j) + 1] = i * Nphi + (j + 1) % Npsi;
            indices[6 * (i * Nphi + j) + 2] = (i + 1) % Nphi * Nphi + j;

            indices[6 * (i * Nphi + j) + 3] = i * Nphi + (j + 1) % Npsi;
            indices[6 * (i * Nphi + j) + 4] = (i + 1) % Nphi * Nphi + (j + 1) % Npsi;
            indices[6 * (i * Nphi + j) + 5] = (i + 1) % Nphi * Nphi + j;
        }
    }

    // paths to shaders
    const std::string filePath = std::source_location::current().file_name();
    const auto pos             = filePath.find("examples");
    const auto path            = std::filesystem::path(filePath.substr(0, pos)).make_preferred();
    const auto shaderPath      = path / "shader";

    // shaders
    const Shader shader{shaderPath / "torus.vert", shaderPath / "torus.frag"};

    // vertex buffer
    unsigned int VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), vertices.data(), GL_STATIC_DRAW);

    // vertex array
    unsigned int VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    // position
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)0);
    glEnableVertexAttribArray(0);
    // texture
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // element buffer
    unsigned int EBO;
    glGenBuffers(1, &EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned), indices.data(), GL_STATIC_DRAW);

    // openGL texture
    unsigned int texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // load texture
    int textureWidth, textureHeight, nrChannels;
    const std::string str = (path / "res" / "face.jpg").string();
    stbi_set_flip_vertically_on_load(true);
    unsigned char *data = stbi_load(str.c_str(), &textureWidth, &textureHeight, &nrChannels, 0);
    if (data) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textureWidth, textureHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    } else {
        throw std::runtime_error{"Failed to load texture"};
    }
    stbi_image_free(data);

    glEnable(GL_DEPTH_TEST);
    // render loop
    for (const auto t0 = std::chrono::steady_clock::now(); !window.shoudClose();) {
        // key input
        processInput(window.pointer);

        // fill background
        glClearColor(0.1f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUseProgram(shader.program);
        // aspect ratio
        int width, height;
        glfwGetFramebufferSize(window.pointer, &width, &height);
        glUniform1f(glGetUniformLocation(shader.program, "uAspectRatio"),
                    static_cast<float>(width) / static_cast<float>(height));

        // time
        const float t = std::chrono::duration<float, std::ratio<1>>(std::chrono::steady_clock::now() - t0).count();
        glUniform1f(glGetUniformLocation(shader.program, "t"), t);

        // draw triangle
        glBindTexture(GL_TEXTURE_2D, texture);
        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
    }

    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);

    shader.terminate();
    window.terminate();

    return 0;
} catch (std::exception &e) {
    std::cout << e.what() << std::endl;
}
