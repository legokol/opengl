#include <array>
#include <filesystem>
#include <iostream>
#include <source_location>

#include "lgklGL/Shader.hpp"
#include "lgklGL/Window.hpp"

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

int main() try {
    const Window window(800, 600, "Triangle");

    // triangle vertices
    const std::array vertices{-0.5f, -0.5f, 0.f, 0.5f, -0.5f, 0.f, 0.0f, 0.5f, 0.f};

    // path to shaders
    const std::string filePath = std::source_location::current().file_name();
    const auto pos             = filePath.find("examples");
    const auto shaderPath      = std::filesystem::path(filePath.substr(0, pos)).make_preferred() / "shader";

    // shaders
    const Shader shader{shaderPath / "simple.vert", shaderPath / "simple.frag"};

    // vertex buffer
    unsigned int VBO;
    glGenBuffers(1, &VBO);

    // vertex array
    unsigned int VAO;
    glGenVertexArrays(1, &VAO);
    // bind vertex array
    glBindVertexArray(VAO);

    // copy vertices in a buffer
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices.data(), GL_STATIC_DRAW);

    // teaching OpenGL to interpret vertex data
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
    glEnableVertexAttribArray(0);

    // render loop
    while (!window.shoudClose()) {
        // key input
        processInput(window.pointer);

        // fill background
        glClearColor(0.1f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // draw triangle
        glUseProgram(shader.program);
        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, 3);
    }

    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);

    shader.terminate();
    window.terminate();

    return 0;
} catch (std::exception &e) {
    std::cout << e.what() << std::endl;
}
