#include <array>
#include <chrono>
#include <filesystem>
#include <iostream>
#include <source_location>

#include "lgklGL/stb_image.h"

#include "lgklGL/Shader.hpp"
#include "lgklGL/Window.hpp"

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

int main() try {
    const Window window(800, 600, "Rotating face");

    // triangle vertices with texture coords
    const std::array vertices{-0.5f, -0.5f, 0.f, 0.f, 0.f, 0.5f, -0.5f, 0.f, 1.f, 0.f, 0.0f, 0.5f, 0.f, 0.5f, 1.f};

    // paths to shaders
    const std::string filePath = std::source_location::current().file_name();
    const auto pos             = filePath.find("examples");
    const auto path            = std::filesystem::path(filePath.substr(0, pos)).make_preferred();
    const auto shaderPath      = path / "shader";

    // shaders
    const Shader shader{shaderPath / "perspective.vert", shaderPath / "perspective.frag"};

    // vertex buffer
    unsigned int VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices.data(), GL_STATIC_DRAW);

    // vertex array
    unsigned int VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    // position
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)0);
    glEnableVertexAttribArray(0);
    // texture
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // openGL texture
    unsigned int texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // load texture
    int textureWidth, textureHeight, nrChannels;
    const std::string str = (path / "res" / "face.jpg").string();
    stbi_set_flip_vertically_on_load(true);
    unsigned char *data = stbi_load(str.c_str(), &textureWidth, &textureHeight, &nrChannels, 0);
    if (data) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textureWidth, textureHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    } else {
        throw std::runtime_error{"Failed to load texture"};
    }
    stbi_image_free(data);

    glEnable(GL_DEPTH_TEST);
    // render loop
    for (const auto t0 = std::chrono::steady_clock::now(); !window.shoudClose();) {
        // key input
        processInput(window.pointer);

        // fill background
        glClearColor(0.1f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUseProgram(shader.program);
        // aspect ratio
        int width, height;
        glfwGetFramebufferSize(window.pointer, &width, &height);
        glUniform1f(glGetUniformLocation(shader.program, "uAspectRatio"),
                    static_cast<float>(width) / static_cast<float>(height));

        // time
        const float t = std::chrono::duration<float, std::ratio<1>>(std::chrono::steady_clock::now() - t0).count();
        glUniform1f(glGetUniformLocation(shader.program, "t"), t);

        // draw triangle
        glBindTexture(GL_TEXTURE_2D, texture);
        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, 3);
    }

    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);

    shader.terminate();
    window.terminate();

    return 0;
} catch (std::exception &e) {
    std::cout << e.what() << std::endl;
}
