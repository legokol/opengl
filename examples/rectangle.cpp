#include <array>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <source_location>

#include "lgklGL/Shader.hpp"
#include "lgklGL/Window.hpp"

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

int main() try {
    const Window window(800, 600, "Rectangle");

    // rectangle (2 triangles) vertices
    const std::array vertices{-0.5f, -0.5f, 0.f, -0.5f, 0.5f, 0.f, 0.5f, 0.5f, 0.f, 0.5f, -0.5f, 0.f};
    // indices
    const std::array indices{0u, 1u, 2u, 2u, 3u, 0u};

    // paths to shaders
    const std::string filePath = std::source_location::current().file_name();
    const auto pos             = filePath.find("examples");
    const auto shaderPath      = std::filesystem::path(filePath.substr(0, pos)).make_preferred() / "shader";

    // shaders
    const Shader shader{shaderPath / "simple.vert", shaderPath / "simple.frag"};

    // vertex buffer
    unsigned int VBO;
    glGenBuffers(1, &VBO);

    // vertex array
    unsigned int VAO;
    glGenVertexArrays(1, &VAO);
    // bind vertex array
    glBindVertexArray(VAO);

    // copy vertices in a buffer
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices.data(), GL_STATIC_DRAW);

    // teaching OpenGL to interpret vertex data
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
    glEnableVertexAttribArray(0);

    // element buffer
    unsigned int EBO;
    glGenBuffers(1, &EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices.data(), GL_STATIC_DRAW);

    // render loop
    while (!window.shoudClose()) {
        // key input
        processInput(window.pointer);

        // fill background
        glClearColor(0.1f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // draw triangle
        glUseProgram(shader.program);
        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    }

    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteProgram(shader.program);

    shader.terminate();
    window.terminate();

    return 0;
} catch (std::exception &e) {
    std::cout << e.what() << std::endl;
}
