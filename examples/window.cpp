#include <iostream>

#include "lgklGL/Window.hpp"

void processInput(GLFWwindow* window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

int main() try {
    const Window window(800, 600, "Window");

    // render loop
    while (!window.shoudClose()) {
        // key input
        processInput(window.pointer);

        glClearColor(0.7f, 0.5f, 0.5f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
    }

    window.terminate();

    return 0;
} catch (std::exception& e) {
    std::cout << e.what() << std::endl;
}